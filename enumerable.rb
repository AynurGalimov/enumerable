module Enumerable
  def my_each
    for s in self
      yield(s)
    end
  end

  def my_each_with_index
    index = 0
    for s in self
      yield(index, s)
      index += 1
    end
  end

  def my_select
    arr = []
    my_each do |s|
      arr << s if yield(s)
    end
    arr
  end

  def my_all?
    for s in self
      return false unless yield(s)
    end

    true
  end

  def my_any?
    for s in self
      return true if yield(s)
    end

    false
  end

  def my_none?
    result = true
    if block_given?
      for s in self
        result = false if yield(s)
      end
    end

    result
  end

  def my_count
    count = 0
    my_each { count += 1 }
    count
  end

  def my_map
    new_arr = []
    for s in self
      new_arr << yield(s)
    end
    new_arr
  end
end

# [1, 2, 3, 4].my_each { |x| puts x * 11 }
# [1, 2, 3, 4].my_each_with_index { |i, x| puts "#{i}: #{x}" }
# a = [1, 2, 3, 4].my_select { |x| x > 2 }
# p a
# p %w[ant bear cat].my_all? { |word| word.length >= 3 }
# p [1, 2, 3, 4].my_all? { |x| x > 0 }
# p %w[ant bear cat].my_any? { |word| word.length >= 4 }
# p %w[ant bear cat].my_none? { |word| word.length == 5 }  #=> true
# p %w[ant bear cat].my_none? { |word| word.length >= 4 }  #=> false
# p [].my_none? #=> true
# p [nil].my_none? #=> true
# p [nil, false].my_none? #=> true
# p [1,2,3,4].my_count
# p [nil, false].my_count
# p [1, 2, 3, 4].my_map { |x| x * 2 }
p %w[a b c d].my_map { |x| x * 2 }
